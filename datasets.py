import imghdr
import pathlib as path

import torch.utils.data as data
from PIL import Image


class ImageDataset(data.Dataset):
    def __init__(self, images_path, transform=None, recursive_search=False, is_file_valid=True, allowed_formats=None):
        super(ImageDataset, self).__init__()
        
        self.images_path = path.Path(images_path)
        self.transform = transform
        
        if not self.images_path.exists():
            raise ValuesError(f"{self.images_path} does not exist")
        
        if not self.images_path.is_dir():
            self.images_paths = self.__load_images_paths_from_file(self.images_path, is_valid=is_file_valid, allowed_formats=allowed_formats)
        elif images_dir is not None:
            self.images_paths = self.__load_images_paths_from_dir(self.images_path, walk=recursive_search, allowed_formats=allowed_formats)
        else:
            raise ValueError('At least one argument should be provided: images_dir, images_file')

    def __len__(self):
        return len(self.images_paths)

    def __getitem__(self, index, color_format='RGB'):
        image = Image.open(str(self.images_paths[index]))
        image = image.convert(color_format)
        if self.transform is not None:
            image = self.transform(image)
        return image

    def __load_images_paths_from_file(self, file, is_valid=True, allowed_formats=None):
        with open(file) as f:
            images_paths = f.readlines()
            
        if is_valid:
            return [x.strip() for x in images_paths]
        else:
            tmp_list = []
            for x in images_paths:
                x = x.strip()
                img_format = imghdr.what(file)
                if img_format is not None and (allowed_formats is None or img_format in allowed_formats):
                    tmp_list.append(x)
            return tmp_list
        
    def __load_images_paths_from_dir(self, images_dir, walk=False, allowed_formats=None):
        images_paths = []
        for file in images_dir.iterdir():
            if walk and file.is_dir():
                images_paths += self.__load_images_paths_from_dir(file, walk, )
            elif file.is_file():
                try:
                    img_format = imghdr.what(file)
                    if img_format is not None and (allowed_formats is None or img_format in allowed_formats):
                        images_paths.append(str(file))
                except PermissionError:
                    pass

        return images_paths
