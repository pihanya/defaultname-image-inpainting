import json
import pathlib as path
from types import SimpleNamespace
import re

config_placeholder_pattern = 'config_{}.json'
config_pattern = re.compile(r'config_\d+.json')

secret_properties=['execution_index']

def compare_configs(config1, config2, ignore_diffs_properties=[], secret_properties=[]):
    if config1 is None or config2 is None:
        raise Exception('Compared configs should not be null')
    
    config1_dict = vars(config1).copy()
    config2_dict = vars(config2).copy()
    
    ignored_diffs = False
    
    for prop in ignore_diffs_properties:
        if not ignored_diffs:
            if prop in config1_dict and prop in config2_dict:
                ignored_diffs = (config1_dict[prop] != config2_dict[prop])
            else:
                ignored_diffs = (prop in config1_dict) or (prop in config2_dict)  
            
        if prop in config1_dict:
            del(config1_dict[prop])
        if prop in config2_dict:
            del(config2_dict[prop])
            
    for prop in secret_properties:
        if prop in config1_dict:
            del(config1_dict[prop])
        if prop in config2_dict:
            del(config2_dict[prop])
    
    equals = (config1_dict == config2_dict)
    return (equals, not equals or ignored_diffs)

def list_configs(log_dir):
    log_dir = path.Path(log_dir)
    if not log_dir.exists():
        return None
    elif not log_dir.is_dir():
        raise Exception(f'{log_dir} is not a directory')
    else:
        return [x for x in log_dir.iterdir() if not x.is_dir() and config_pattern.fullmatch(x.name)]

    
def get_latest_config(log_dir, hide_secret_properties=True):
    max_execution_index = -1
    latest_config = None
    configs = list_configs(log_dir)
    for config_file in configs:
        with open(config_file, 'r') as f:
            config = SimpleNamespace(**json.load(f))
            if 'execution_index' in vars(config) and config.execution_index is not None and config.execution_index > max_execution_index:
                max_execution_index = config.execution_index
                latest_config = config
    
    if hide_secret_properties and latest_config is not None:
        for prop in secret_properties:
            if prop in vars(latest_config):
                del(vars(latest_config)['execution_index'])
   
    return latest_config

    
def log_config(config, log_dir, ignore_diffs_properties=[]):
    log_dir = path.Path(log_dir)
    if not log_dir.exists():
        log_dir.mkdir(parents=True, exist_ok=True)
    elif not log_dir.is_dir():
        raise Exception(f'{log_dir} is not a directory')
    
    if config is None:
        raise Exception(f'Given config is None')
    
    config = SimpleNamespace(**vars(config))
    prev_config = get_latest_config(log_dir, hide_secret_properties=False)
    if prev_config is None:
        config.execution_index = 0
        equals, diffs = (False, True)
    else:
        if 'execution_index' in vars(prev_config):
            config.execution_index = prev_config.execution_index
        else:
            config.execution_index = 0
        equals, diffs = compare_configs(
        prev_config,
        config,
        ignore_diffs_properties=ignore_diffs_properties, 
        secret_properties=secret_properties
        )
    
    # If not ignored properties of config have changed then increase index of config 
    if not equals:
        config.execution_index += 1

    if not equals or diffs:
        config_path = log_dir / config_placeholder_pattern.format(config.execution_index)
        with open(config_path, mode='w') as f:
            json.dump(vars(config), f)
    return config