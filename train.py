from tqdm import tqdm
from models import CompletionNetwork, ContextDiscriminator
from datasets import ImageDataset
from losses import completion_network_loss
from utils import (
    gen_input_mask,
    gen_hole_area,
    crop,
    sample_random_batch,
    poisson_blend,
)
from torch.utils.data import DataLoader
from torch.optim import Adadelta, Adam
from torch.nn import BCELoss, DataParallel
from torchvision.utils import save_image
from PIL import Image
import torchvision.transforms as transforms
import torch
import pathlib as path
import argparse
import numpy as np
import pandas as pd
import json
from logging_utils import log_config, get_latest_config
from datetime import timedelta, datetime

parser = argparse.ArgumentParser()
parser.add_argument('train_dataset')
parser.add_argument('test_dataset')
parser.add_argument('result_dir')
parser.add_argument('--log-dir', dest='log_dir', type=str, default='./log')
parser.add_argument('--recursive_search', action='store_true', default=False)
parser.add_argument('--init_model_cn', type=str, default=None)
parser.add_argument('--init_model_cd', type=str, default=None)
parser.add_argument('--steps_1', type=int, default=90000)
parser.add_argument('--steps_2', type=int, default=10000)
parser.add_argument('--steps_3', type=int, default=400000)
parser.add_argument('--snaperiod_1', type=int, default=10000)
parser.add_argument('--snaperiod_2', type=int, default=2000)
parser.add_argument('--snaperiod_3', type=int, default=10000)
parser.add_argument('--max_holes', type=int, default=1)
parser.add_argument('--hole_min_w', type=int, default=48)
parser.add_argument('--hole_max_w', type=int, default=96)
parser.add_argument('--hole_min_h', type=int, default=48)
parser.add_argument('--hole_max_h', type=int, default=96)
parser.add_argument('--cn_input_size', type=int, default=160)
parser.add_argument('--ld_input_size', type=int, default=96)
parser.add_argument('--optimizer', type=str, choices=['adadelta', 'adam'], default='adadelta')
parser.add_argument('--bsize', type=int, default=16)
parser.add_argument('--bdivs', type=int, default=1)
parser.add_argument('--data_parallel', action='store_true')
parser.add_argument('--logging', action='store_true')
parser.add_argument('--num_test_completions', type=int, default=16)
parser.add_argument('--mpv', nargs=3, type=float, default=None)
parser.add_argument('--alpha', type=float, default=4e-4)
parser.add_argument('--arc', type=str, choices=['celeba', 'places2'], default='celeba')
parser.add_argument('--device', type=str, default='cpu')

df_columns = ['Step', 'CN_Loss', 'CD_Loss', 'Execution_Time', 'Execution_Date', 'Start_Date', 'Description']
config_ignore_diffs_properties = ['stepsctr_1, stepsctr_2, stepsctr_3', 'start_date']

def main(args):
    # ====================
    # Preparation
    # ====================
    start_date = datetime.now()
    args.start_date = start_date.timestamp()
    
    train_dataset = path.Path(args.train_dataset)
    test_dataset = path.Path(args.test_dataset)
    result_dir = path.Path(args.result_dir)
    log_dir = path.Path(args.log_dir)

    if args.init_model_cn is not None:
        args.init_model_cn = str(path.Path(args.init_model_cn))
    if args.init_model_cd is not None:
        args.init_model_cd = str(path.Path(args.init_model_cd))

    # Checking GPU availability
    if not torch.cuda.is_available():
        raise Exception('At least one gpu must be available.')
    else:
        gpu = torch.device(args.device)

    result_dir.mkdir(parents=True, exist_ok=True)
    for s in ['phase_1', 'phase_2', 'phase_3']:
        (result_dir / s).mkdir(parents=True, exist_ok=True)

    # dataset
    transform = transforms.Compose([
        transforms.Resize(args.cn_input_size),
        transforms.RandomCrop((args.cn_input_size, args.cn_input_size)),
        transforms.ToTensor(),
    ])
    print('loading dataset... (it may take a few minutes)')
    
    train_dataset = ImageDataset(train_dataset, transform=transform, recursive_search=args.recursive_search)
    test_dataset = ImageDataset(test_dataset, transform=transform, recursive_search=args.recursive_search)
    train_loader = DataLoader(train_dataset, batch_size=(args.bsize // args.bdivs), shuffle=True)

    # compute mean pixel value of training dataset
    mpv = np.zeros(shape=(3,))
    if args.mpv is None:
        pbar = tqdm(total=len(train_dataset.images_paths), desc='computing mean pixel value for training dataset...')
        for image_path in train_dataset.images_paths:
            img = Image.open(image_path)
            x = np.array(img, dtype=np.float32) / 255.
            mpv += x.mean(axis=(0, 1))
            pbar.update()
        mpv /= len(train_dataset.images_paths)
        pbar.close()
    else:
        mpv = np.array(args.mpv)

    # save training config
    mpv_json = []
    for i in range(3):
        mpv_json.append(float(mpv[i]))  # convert to json serializable type
    args_dict = vars(args)
    args_dict['mpv'] = mpv_json
    with open(result_dir / 'config.json', mode='w') as f:
        json.dump(args_dict, f)

    config = get_latest_config(args.log_dir)
    if config is not None:
        if 'stepsctr_1' in vars(config): args.stepsctr_1 = config.stepsctr_1
        else: args.stepsctr_1 = 0
        if 'stepsctr_2' in vars(config): args.stepsctr_2 = config.stepsctr_2
        else: args.stepsctr_2 = 0
        if 'stepsctr_3' in vars(config): args.stepsctr_3 = config.stepsctr_3
        else: args.stepsctr_3 = 0
    else:
        args.stepsctr_1 = 0
        args.stepsctr_2 = 0
        args.stepsctr_3 = 0

    log_config(args, args.log_dir, ignore_diffs_properties=['stepsctr_1, stepsctr_2, stepsctr_3'])

    # make mpv & alpha tensor
    mpv = torch.tensor(mpv.astype(np.float32).reshape(1, 3, 1, 1)).to(gpu)
    alpha = torch.tensor(args.alpha).to(gpu)

    if args.logging:
        train_history_file = log_dir / 'train_history.csv'
        if train_history_file.exists():
            df = pd.read_csv(train_history_file,
                             dtype={
                                 'Step': 'int64',
                                 'CN_Loss': 'float64',
                                 'CD_Loss': 'float64',
                                 'Execution_Time': 'int64',
                                 'Execution_Date': 'datetime64[ns]',
                                 'Start_Date': 'datetime64[ns]',
                                 'Description': 'object'
                             }
                            )[df_columns]
        else: 
            df = pd.DataFrame(columns=df_columns)
            df = df.to_csv(train_history_file)
        
        df['Step'] = df['Step'].astype('int64')
        df['CN_Loss'] = df['CN_Loss'].astype('float64')
        df['CD_Loss'] = df['CN_Loss'].astype('float64')
        df['Execution_Time'] = df['Execution_Time'].astype('int64')
        df['Execution_Date'] = df['Execution_Date'].astype('datetime64[ns]')
        df['Start_Date'] = df['Start_Date'].astype('datetime64[ns]')
        df['Description'] = df['Description'].astype('object')

    # ====================
    # Training Phase 1
    # ====================
    model_cn = CompletionNetwork()
    if args.data_parallel:
        model_cn = DataParallel(model_cn)
    if args.init_model_cn is not None:
        model_cn.load_state_dict(torch.load(args.init_model_cn, map_location='cpu'))
    if args.optimizer == 'adadelta':
        opt_cn = Adadelta(model_cn.parameters())
    else:
        opt_cn = Adam(model_cn.parameters())
    model_cn = model_cn.to(gpu)

    # training
    cnt_bdivs = 0
    pbar = tqdm(total=args.steps_1, initial=args.stepsctr_1)
    while pbar.n < args.steps_1:
        for x in train_loader:
            if args.logging:
                epoch_start = datetime.now()
            
            # forward
            x = x.to(gpu)
            mask = gen_input_mask(
                shape=(x.shape[0], 1, x.shape[2], x.shape[3]),
                hole_size=((args.hole_min_w, args.hole_max_w), (args.hole_min_h, args.hole_max_h)),
                hole_area=gen_hole_area((args.ld_input_size, args.ld_input_size), (x.shape[3], x.shape[2])),
                max_holes=args.max_holes,
            ).to(gpu)
            x_mask = x - x * mask + mpv * mask
            input = torch.cat((x_mask, mask), dim=1)
            output = model_cn(input)
            loss = completion_network_loss(x, output, mask)

            # backward
            loss.backward()
            cnt_bdivs += 1

            if cnt_bdivs >= args.bdivs:
                cnt_bdivs = 0
                # optimize
                opt_cn.step()
                # clear grads
                opt_cn.zero_grad()
                # update progbar
                pbar.set_description('phase 1 | train loss: %.8f' % loss.cpu())
                pbar.update()
                # test
                if pbar.n % args.snaperiod_1 == 0:
                    with torch.no_grad():
                        x = sample_random_batch(test_dataset, batch_size=args.num_test_completions).to(gpu)
                        mask = gen_input_mask(
                            shape=(x.shape[0], 1, x.shape[2], x.shape[3]),
                            hole_size=((args.hole_min_w, args.hole_max_w), (args.hole_min_h, args.hole_max_h)),
                            hole_area=gen_hole_area((args.ld_input_size, args.ld_input_size), (x.shape[3], x.shape[2])),
                            max_holes=args.max_holes,
                        ).to(gpu)
                        x_mask = x - x * mask + mpv * mask
                        input = torch.cat((x_mask, mask), dim=1)
                        output = model_cn(input)
                        completed = poisson_blend(x, output, mask)
                        imgs = torch.cat((x.cpu(), x_mask.cpu(), completed.cpu()), dim=0)
                        image_path = result_dir / 'phase_1' / f"step{pbar.n}.png"
                        model_cn_path = result_dir / 'phase_1' / f"model_cn_step{pbar.n}"
                        save_image(imgs, image_path, nrow=len(x))
                        torch.save(model_cn.state_dict(), model_cn_path)
                        
                    args_dict['stepsctr_1'] = pbar.n
                    log_config(args, args.log_dir, ignore_diffs_properties=config_ignore_diffs_properties)
                    if args.logging:
                        df.to_csv(train_history_file)
                    
                # terminate
                if pbar.n >= args.steps_1:
                    break
        if args.logging:
            epoch_end = datetime.now()
            df = df.append(
                {
                    'Step': pbar.n,
                    'CN_Loss': str(loss),
                    'CD_Loss': None,
                    'Execution_Time': (epoch_end - epoch_start).microseconds,
                    'Execution_Date': datetime.now(),
                    'Start_Date': start_date,
                    'Description': 'phase_1'
                }, ignore_index=True
            )
    pbar.close()

    # ====================
    # Training Phase 2
    # ====================
    model_cd = ContextDiscriminator(
        local_input_shape=(3, args.ld_input_size, args.ld_input_size),
        global_input_shape=(3, args.cn_input_size, args.cn_input_size),
        arc=args.arc,
    )
    if args.data_parallel:
        model_cd = DataParallel(model_cd)
    if args.init_model_cd is not None:
        model_cd.load_state_dict(torch.load(args.init_model_cd, map_location='cpu'))
    if args.optimizer == 'adadelta':
        opt_cd = Adadelta(model_cd.parameters())
    else:
        opt_cd = Adam(model_cd.parameters())
    model_cd = model_cd.to(gpu)
    bceloss = BCELoss()

    # training
    cnt_bdivs = 0
    pbar = tqdm(total=args.steps_2, initial=args.stepsctr_2)
    while pbar.n < args.steps_2:
        for x in train_loader:
            if args.logging:
                epoch_start = datetime.now()

            # fake forward
            x = x.to(gpu)
            hole_area_fake = gen_hole_area((args.ld_input_size, args.ld_input_size), (x.shape[3], x.shape[2]))
            mask = gen_input_mask(
                shape=(x.shape[0], 1, x.shape[2], x.shape[3]),
                hole_size=((args.hole_min_w, args.hole_max_w), (args.hole_min_h, args.hole_max_h)),
                hole_area=hole_area_fake,
                max_holes=args.max_holes,
            ).to(gpu)
            fake = torch.zeros((len(x), 1)).to(gpu)
            x_mask = x - x * mask + mpv * mask
            input_cn = torch.cat((x_mask, mask), dim=1)
            output_cn = model_cn(input_cn)
            input_gd_fake = output_cn.detach()
            input_ld_fake = crop(input_gd_fake, hole_area_fake)
            output_fake = model_cd((input_ld_fake.to(gpu), input_gd_fake.to(gpu)))
            loss_fake = bceloss(output_fake, fake)

            # real forward
            hole_area_real = gen_hole_area(size=(args.ld_input_size, args.ld_input_size),
                                           mask_size=(x.shape[3], x.shape[2]))
            real = torch.ones((len(x), 1)).to(gpu)
            input_gd_real = x
            input_ld_real = crop(input_gd_real, hole_area_real)
            output_real = model_cd((input_ld_real, input_gd_real))
            loss_real = bceloss(output_real, real)

            # reduce
            loss = (loss_fake + loss_real) / 2.

            # backward
            loss.backward()
            cnt_bdivs += 1

            if cnt_bdivs >= args.bdivs:
                cnt_bdivs = 0
                # optimize
                opt_cd.step()
                # clear grads
                opt_cd.zero_grad()
                # update progbar
                pbar.set_description('phase 2 | train loss: %.8f' % loss.cpu())
                pbar.update()



                # test
                if pbar.n % args.snaperiod_2 == 0:
                    with torch.no_grad():
                        x = sample_random_batch(test_dataset, batch_size=args.num_test_completions).to(gpu)
                        mask = gen_input_mask(
                            shape=(x.shape[0], 1, x.shape[2], x.shape[3]),
                            hole_size=((args.hole_min_w, args.hole_max_w), (args.hole_min_h, args.hole_max_h)),
                            hole_area=gen_hole_area((args.ld_input_size, args.ld_input_size), (x.shape[3], x.shape[2])),
                            max_holes=args.max_holes,
                        ).to(gpu)
                        x_mask = x - x * mask + mpv * mask
                        input = torch.cat((x_mask, mask), dim=1)
                        output = model_cn(input)
                        completed = poisson_blend(x, output, mask)
                        imgs = torch.cat((x.cpu(), x_mask.cpu(), completed.cpu()), dim=0)
                        image_path = result_dir / 'phase_2' / f"step{pbar.n}.png"
                        model_cd_path = result_dir / 'phase_2' / f"model_cd_step{pbar.n}"
                        save_image(imgs, image_path, nrow=len(x))
                        torch.save(model_cd.state_dict(), model_cd_path)
                        
                    args_dict['stepsctr_2'] = pbar.n
                    log_config(args, args.log_dir, ignore_diffs_properties=config_ignore_diffs_properties)
                    if args.logging:
                        df.to_csv(train_history_file)
                # terminate
                if pbar.n >= args.steps_2:
                    break

                if args.logging:
                    epoch_end = datetime.now()
                    df = df.append(
                        {
                            'Step': pbar.n,
                            'CN_Loss': None,
                            'CD_Loss': str(loss),
                            'Execution_Time': (epoch_end - epoch_start).microseconds,
                            'Execution_Date': datetime.now(),
                            'Start_Date': start_date,
                            'Description': 'phase_2'
                        }, ignore_index=True
                    )
    pbar.close()

    # ====================
    # Training Phase 3
    # ====================
    # training
    cnt_bdivs = 0
    pbar = tqdm(total=args.steps_3, initial=args.stepsctr_3)
    while pbar.n < args.steps_3:
        for x in train_loader:
            if args.logging:
                epoch_start = datetime.now()

            # forward model_cd
            x = x.to(gpu)
            hole_area_fake = gen_hole_area((args.ld_input_size, args.ld_input_size), (x.shape[3], x.shape[2]))
            mask = gen_input_mask(
                shape=(x.shape[0], 1, x.shape[2], x.shape[3]),
                hole_size=((args.hole_min_w, args.hole_max_w), (args.hole_min_h, args.hole_max_h)),
                hole_area=hole_area_fake,
                max_holes=args.max_holes,
            ).to(gpu)

            # fake forward
            fake = torch.zeros((len(x), 1)).to(gpu)
            x_mask = x - x * mask + mpv * mask
            input_cn = torch.cat((x_mask, mask), dim=1)
            output_cn = model_cn(input_cn)
            input_gd_fake = output_cn.detach()
            input_ld_fake = crop(input_gd_fake, hole_area_fake)
            output_fake = model_cd((input_ld_fake, input_gd_fake))
            loss_cd_fake = bceloss(output_fake, fake)

            # real forward
            hole_area_real = gen_hole_area(size=(args.ld_input_size, args.ld_input_size),
                                           mask_size=(x.shape[3], x.shape[2]))
            real = torch.ones((len(x), 1)).to(gpu)
            input_gd_real = x
            input_ld_real = crop(input_gd_real, hole_area_real)
            output_real = model_cd((input_ld_real, input_gd_real))
            loss_cd_real = bceloss(output_real, real)

            # reduce
            loss_cd = (loss_cd_fake + loss_cd_real) * alpha / 2.

            # backward model_cd
            loss_cd.backward()

            cnt_bdivs += 1
            if cnt_bdivs >= args.bdivs:
                # optimize
                opt_cd.step()
                # clear grads
                opt_cd.zero_grad()

            # forward model_cn
            loss_cn_1 = completion_network_loss(x, output_cn, mask)
            input_gd_fake = output_cn
            input_ld_fake = crop(input_gd_fake, hole_area_fake)
            output_fake = model_cd((input_ld_fake, (input_gd_fake)))
            loss_cn_2 = bceloss(output_fake, real)

            # reduce
            loss_cn = (loss_cn_1 + alpha * loss_cn_2) / 2.

            # backward model_cn
            loss_cn.backward()

            if cnt_bdivs >= args.bdivs:
                cnt_bdivs = 0
                # optimize
                opt_cn.step()
                # clear grads
                opt_cn.zero_grad()
                # update progbar
                pbar.set_description('phase 3 | train loss (cd): %.8f (cn): %.8f' % (loss_cd.cpu(), loss_cn.cpu()))
                pbar.update()

                # test
                if pbar.n % args.snaperiod_3 == 0:
                    with torch.no_grad():
                        x = sample_random_batch(test_dataset, batch_size=args.num_test_completions).to(gpu)
                        mask = gen_input_mask(
                            shape=(x.shape[0], 1, x.shape[2], x.shape[3]),
                            hole_size=((args.hole_min_w, args.hole_max_w), (args.hole_min_h, args.hole_max_h)),
                            hole_area=gen_hole_area((args.ld_input_size, args.ld_input_size), (x.shape[3], x.shape[2])),
                            max_holes=args.max_holes,
                        ).to(gpu)
                        x_mask = x - x * mask + mpv * mask
                        input = torch.cat((x_mask, mask), dim=1)
                        output = model_cn(input)
                        completed = poisson_blend(x, output, mask)
                        imgs = torch.cat((x.cpu(), x_mask.cpu(), completed.cpu()), dim=0)
                        image_path = result_dir / 'phase_3' / f"step{pbar.n}.png"
                        model_cn_path = result_dir / 'phase_3' / f"model_cn_step{pbar.n}"
                        model_cd_path = result_dir / 'phase_3' / f"model_cd_step{pbar.n}"
                        save_image(imgs, image_path, nrow=len(x))
                        torch.save(model_cn.state_dict(), model_cn_path)
                        torch.save(model_cd.state_dict(), model_cd_path)
                    args_dict['stepsctr_3'] = pbar.n
                    log_config(args, args.log_dir, ignore_diffs_properties=config_ignore_diffs_properties)
                    if args.logging:
                        df.to_csv(train_history_file)
                # terminate
                if pbar.n >= args.steps_3:
                    break

                if args.logging:
                    epoch_end = datetime.now()
                    df = df.append(
                        {
                            'Step': pbar.n,
                            'CN_Loss': str(loss_cn),
                            'CD_Loss': str(loss_cd),
                            'Execution_Time': (epoch_end - epoch_start).microseconds,
                            'Execution_Date': datetime.now(),
                            'Start_Date': start_date,
                            'Description': 'phase_3'
                        }, ignore_index=True
                    )
    pbar.close()


if __name__ == '__main__':
    args = parser.parse_args()
    main(args)
