python3 train.py \
        ./data/train_dataset.txt \
        ./data/test_dataset.txt \
        ./result \
        --init_model_cn ./result/phase_1/model_cn \
        --init_model_cd ./result/phase_2/model_cd \
        --arc places2 --optimizer adam \
        --steps_1 0 --steps_2 10000 --steps_3 0 \
        --snaperiod_1 1000 --snaperiod_2 2500 --snaperiod_3 2000 \
        --num_test_completions 32\
        --max_holes 3 \
        --hole_min_w 50 --hole_min_h 50\
        --hole_max_w 60 --hole_max_h 60 \
        --bsize 8 --bdivs 1 \
        --cn_input_size 256 --ld_input_size 160 \
        --device cuda:4 \
        --mpv 0.45797884424150065 0.44116137439592373 0.4077408466382233
